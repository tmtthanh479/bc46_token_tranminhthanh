import { RouteObject } from "react-router-dom";
import Demo from "../demo/Demo";
import { PATH } from "constant";
import { AuthLayout } from "components/layouts";



export const router: RouteObject[] = [
  {
    path: "/demo",
    element: <Demo />,
  },
  {
    element: <AuthLayout/>,
    children: [


    ],
  },
];
