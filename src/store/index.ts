import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "./rootReducer";
import { useDispatch } from "react-redux";

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<(typeof store)["getState"]>;

type AppDispath = (typeof store)["dispatch"];
export const useAppDispath: () => AppDispath = useDispatch; // khi sử dụng redux tookkit(typescript) thì phải dùng useAppDispath định nghĩa thay vì useDispatch
