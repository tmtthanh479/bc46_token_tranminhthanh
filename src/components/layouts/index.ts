export * from "./AuthLayout";


// I. Chức năng làm gọn đường dẫn import pử router/index.tsx

// =============================== config cho vscode:
// Bước 1: vào tsconfig.json trong "light" thêm:
/*
// custom cho import ngắn lại
    "noFallthroughCasesInSwitch": true,
    "baseUrl": "src",
    "paths": {
      // custom cho import ngắn lại | ghi ra tất cả các name folder || chi tiết index.ts
      "assets": ["assets"],
      "components": ["components"],
      "constans": ["constans"],
      "demo": ["demo"],
      "hooks": ["hooks"],
      "pages": ["pages"],
      "router": ["router"],
      "services": ["services"],
      "store": ["store"],
      "types": ["types"],
      "utils" : ["utils"],
*/
// Bước 2: Trong mỗi folder phải tạo "index.ts" và thêm 2 dòng export đầu tiên
// Bước 3: Trong mỗi file ".tsx" Thêm export trước const || hoặc xóa export default
// Bước 4: Vào router nhớ import { Login, Register } from "pages";
// Bước 5: cài package: yarn add @types/node

// =============================== config cho terminal:
// Bước 1: vào vite.config dưới
/*
 server: {
    port: 3000,
  },
  // congfig ở đây đường dẫn
    resolve:{
    alias:{
      "@": path.resolve(__dirname,"./src"),
      "assets": path.resolve(__dirname,"src/assets"),
      "components": path.resolve(__dirname,"src/components"),
      "constans": path.resolve(__dirname,"src/constans"),
      "demo": path.resolve(__dirname,"src/demo"),
      "hooks": path.resolve(__dirname,"src/hooks"),
      "pages": path.resolve(__dirname,"src/pages"),
      "router": path.resolve(__dirname,"src/router"),
      "services": path.resolve(__dirname,"src/services"),
      "store": path.resolve(__dirname,"src/store"),
      "types": path.resolve(__dirname,"src/types"),
      "utils": path.resolve(__dirname,"src/utils"),
    }
  }
*/
